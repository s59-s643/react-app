import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';



export default function AppNavbar(){

	return (
		<Navbar bg="light" expand="lg">
	      <Container fluid>
	        <Navbar.Brand href="#home">GamePlay</Navbar.Brand>
	        <Navbar.Toggle aria-controls="navbarScroll" />
	        <Navbar.Collapse id="navbarScroll">
	          <Nav className="ml-auto">
	          	  <Nav.Link as={Link} to="/">Home</Nav.Link>
		          <Nav.Link as={Link} to="/register">Register</Nav.Link>
		          <Nav.Link as={Link} to="/login" >Login</Nav.Link>
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>

	)
}