
//import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

//COMPONENTS
import AppNavbar from './components/AppNavbar';


//PAGES
import Home from './pages/Home';
import Login from './pages/Login'
import Register from './pages/Register';
import './App.css';



function App() {

  return (

    <Router>
      <AppNavbar />
      <Container>
        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/login" element={<Login/>} />
        </Routes>
      </Container>
    </Router>
    
  );
}

export default App;
