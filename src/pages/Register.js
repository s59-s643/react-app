import { Form, Button } from 'react-bootstrap';




export default function Register() {

	return (
		<Form>
		<h1>Register</h1>

		  <Form.Group className="mb-3" controlId="fistName">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control type="text" placeholder="Enter your First Name" />
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="lastName">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control type="text" placeholder="Enter your Last Name" />
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter your email" />
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="mobileNo">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control type="text" placeholder="+63 XXXXXXXXXX" />
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" />
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="verifyPassword">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" />
	      </Form.Group>

	      <Button variant="primary" type="submit">
	        Submit
	      </Button>
	    </Form>
	)


}