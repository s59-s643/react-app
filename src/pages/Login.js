import { Form, Button } from 'react-bootstrap';

export default function Login() {

	return (
		<Form>
		<h1>Login</h1>

	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter your email" />
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" />
	      </Form.Group>

	      <Button variant="primary" type="submit">
	        Login
	      </Button>
	    </Form>
	)


}